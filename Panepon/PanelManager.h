//
//  PanelManager.h
//  Panepon
//
//  Created by yokota on 2014/07/07.
//  Copyright (c) 2014年 job2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PanelManager : NSObject
@property (nonatomic) int **panelStatus;
-(BOOL)isPanelClear;
-(BOOL)isSimultaneousClear;
-(BOOL)fallCheck;
-(BOOL)fallPanel;
-(void)panelSwap:(int)panel_x :(int)panel_y;
@end
