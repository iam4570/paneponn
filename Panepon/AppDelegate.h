//
//  AppDelegate.h
//  Panepon
//
//  Created by yokota on 2014/07/07.
//  Copyright (c) 2014年 job2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
