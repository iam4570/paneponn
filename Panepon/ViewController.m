//
//  ViewController.m
//  Panepon
//
//  Created by yokota on 2014/07/07.
//  Copyright (c) 2014年 job2. All rights reserved.
//

#import "ViewController.h"
#import "PanelManager.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *panelContainer;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic)PanelManager *panelManager;
@property (nonatomic)int panelWidth;
@property (nonatomic)int panelHeight;
@property (nonatomic)NSTimer *gameTimer;
@property (nonatomic) CGPoint touchPanel;
@end
const int numOfPanel = 4;
@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //パネルの最大の高さと幅
    self.panelWidth = 6;
    self.panelHeight = 10;
    //パネルを保存する二次元配列の確保
    self.panelManager = [[PanelManager alloc]init];
    for (int i=0; i<self.panelHeight; i++) {
        for (int j=0; j<self.panelWidth; j++) {
            UIView *newView = [[UIView alloc]initWithFrame:CGRectMake(j*40, i*40, 40, 40)];
            newView.tag=j*self.panelHeight+i+1;
            int random_number = arc4random()%numOfPanel+1;
            self.panelManager.panelStatus[j+1][i+1]=random_number;
            
            if (random_number == 6) {
                [newView setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
            }else if(random_number==1){
                [newView setBackgroundColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
            }else if(random_number==2){
                [newView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1]];
            }else if(random_number==3){
                [newView setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:1 alpha:1]];
            }else if(random_number==4){
                [newView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:1]];
            }else if(random_number==5){
                [newView setBackgroundColor:[UIColor colorWithRed:0 green:1 blue:1 alpha:1]];
            }else if(random_number>10 && random_number<20){
                [newView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
            }
            [self.panelContainer addSubview:newView];
        }
    }
    //タイマーの初期化
    self.gameTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(panelLoop) userInfo:nil repeats:YES];
}
-(void)panelLoop
{
    
                [self.panelManager fallCheck];
                [self.panelManager isSimultaneousClear];
                
                [self panelRedraw];
                [NSThread sleepForTimeInterval:1.0];
                [self.panelManager isPanelClear];
                [self.panelManager fallPanel];
                [self panelRedraw];
                [NSThread sleepForTimeInterval:1.0];
    
}

- (IBAction)testButton:(id)sender {
    switch ([sender tag]) {
        case 100:
            [self.gameTimer fire];
            break;
        case 101:
            [self.panelManager isSimultaneousClear];
            [self panelRedraw];
            
            break;
        case 102:
            [self.panelManager fallCheck];
            break;
        case 103:
            [self.panelManager fallPanel];
            [self panelRedraw];
            break;
        case 104:
            [self.gameTimer invalidate];
            self.gameTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(panelLoop) userInfo:nil repeats:YES];
            [self panelReset];
            [self panelRedraw];
            break;
            
        default:
            break;
    }
}
- (void)fallCheck {
    [self.panelManager fallCheck];
}
-(void)panelClear{
    [self.panelManager isSimultaneousClear];
}
-(void)clearJudge{
    [self.panelManager isPanelClear];
}
-(void)fallPanel{
    [self.panelManager fallPanel];
}
-(void)panelReset
{
    for (int i=0; i<self.panelHeight; i++) {
        for (int j=0; j<self.panelWidth; j++) {
            self.panelManager.panelStatus[j+1][i+1] = arc4random()%numOfPanel+1;
        }
    }
}
-(void)panelRedraw
{
    for (int i=0; i<self.panelHeight; i++) {
        for (int j=0; j<self.panelWidth; j++) {
            int status =self.panelManager.panelStatus[j+1][i+1];
            UIView *panelView = [self.view viewWithTag:j*self.panelHeight+i+1];
            if (status==6) {
                [panelView setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
            }else if(status==1){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
            }else if(status==2){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1]];
            }else if(status==3){
                [panelView setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:1 alpha:1]];
            }else if(status==4){
                [panelView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:1]];
            }else if(status==5){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:1 blue:1 alpha:1]];
            }else if(status==11){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0.3 blue:0 alpha:0.5]];
            }else if(status==12){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0.3 alpha:0.5]];
            }else if(status==13){
                [panelView setBackgroundColor:[UIColor colorWithRed:0.3 green:0 blue:0.3 alpha:0.5]];
            }else if(status==14){
                [panelView setBackgroundColor:[UIColor colorWithRed:0.3 green:0.3 blue:0 alpha:0.5]];
            }else if(status==15){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0.3 blue:0.3 alpha:0.5]];
            }else if(status==16){
                [panelView setBackgroundColor:[UIColor colorWithRed:0.3 green:0 blue:0 alpha:0.5]];
            }else if(status==20){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
            }else if(status==21){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0.3 blue:0 alpha:0.8]];
            }else if(status==22){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0.3 alpha:0.8]];
            }else if(status==23){
                [panelView setBackgroundColor:[UIColor colorWithRed:0.3 green:0 blue:0.3 alpha:0.8]];
            }else if(status==24){
                [panelView setBackgroundColor:[UIColor colorWithRed:0.3 green:0.3 blue:0 alpha:0.8]];
            }else if(status==25){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0.3 blue:0.3 alpha:0.8]];
            }else if(status==26){
                [panelView setBackgroundColor:[UIColor colorWithRed:0.3 green:0 blue:0 alpha:0.8]];
            }else if(status>20 && status<30){
                [panelView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
            }else if(status==0){
                [panelView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
            }
            
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint pt = [[touches anyObject] locationInView:[self.view viewWithTag:200]];
    //タッチされたパネルの位置を取得
    self.touchPanel= CGPointMake( (int)pt.x/40+1,(int)pt.y/40+1);
    NSLog(@"(%f,%f)touched",self.touchPanel.x,self.touchPanel.y);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint pt = [[touches anyObject] locationInView:[self.view viewWithTag:200]];
    //移動した先のパネルの位置を取得
    CGPoint movedPanel =CGPointMake( (int)pt.x/40+1,(int)pt.y/40+1);
    NSLog(@"(%f,%f) to (%f,%f)",self.touchPanel.x,self.touchPanel.y,movedPanel.x,movedPanel.y);
    NSLog(@"(%f,%f)moved",pt.x,pt.y);
    if (movedPanel.x == self.touchPanel.x+1) {
        [self.panelManager panelSwap:self.touchPanel.x:self.touchPanel.y];
        self.touchPanel = movedPanel;
    }else if(movedPanel.x == self.touchPanel.x-1){
        [self.panelManager panelSwap:movedPanel.x :movedPanel.y];
        self.touchPanel = movedPanel;
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self panelRedraw];
    self.touchPanel = CGPointMake(-1, -1);
    NSLog(@"touchEnded");
}
@end
