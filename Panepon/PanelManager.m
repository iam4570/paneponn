//
//  PanelManager.m
//  Panepon
//
//  Created by yokota on 2014/07/07.
//  Copyright (c) 2014年 job2. All rights reserved.
//

#import "PanelManager.h"

const int panelHeight = 10;
const int panelWidth = 6;
@interface PanelManager()

@end
@implementation PanelManager

-(id)init
{
    self = [super init];
    if (self) {
        //--パネルの状態を表す二次元配列の確保--
        //壁と高さの分の領域を確保
        self.panelStatus = (int**)malloc(sizeof(int*)*(panelWidth+2));
        //幅の分を確保
        for (int i = 0; i<panelWidth+2; i++) {
            int *array = (int*)malloc(sizeof(int)*(panelHeight+2));
            //初期値を設定,壁=-1,内部=0
            for (int j=0; j<panelHeight+2; j++) {
                if (i==0 || i==panelWidth+1) {
                    array[j]=-1;
                }
                else if (j==0 || j==panelHeight+1) {
                    array[j]=-1;
                }
                else{
                    array[j]=0;
                }
            }
            self.panelStatus[i]=array;
        }
//        [self logPanel];
        
    }
    
    return self;
}
//パネルが消えるかどうかの判定
-(BOOL)isPanelClear
{
    BOOL ans = NO;
    for (int i=1; i<panelHeight+1; i++) {
        for (int j=1; j<panelWidth+1; j++) {
            int connect = 0;
            connect = [self searchPanel:j:i];
//            [self logPanel];
            if (connect == 1) {
                self.panelStatus[j][i]-=10;
            }
            else if (connect <3) {
                self.panelStatus[j][i]-=10;
//                [self cancelClear:j :i];
            }else{
                ans=YES;
            }
//            [self logPanel];
        }
    }
    return ans;
}
-(void)cancelClear:(int)panel_x :(int)panel_y
{
    int currentStatus = self.panelStatus[panel_x][panel_y];
    int rightStatus = self.panelStatus[panel_x+1][panel_y];
    int leftStatus = self.panelStatus[panel_x-1][panel_y];
    int upStatus = self.panelStatus[panel_x][panel_y-1];
    int downStatus = self.panelStatus[panel_x][panel_y+1];
    
    //チェック済みフラグを消す
    self.panelStatus[panel_x][panel_y]-=10;
    //色が同じかどうかを判定する
    //右判定
    if (currentStatus == rightStatus) {
        [self cancelClear:panel_x+1 :panel_y];
    }
    //下判定
    if (currentStatus == downStatus) {
        [self cancelClear:panel_x :panel_y+1];
    }
    //左判定
    if (currentStatus == leftStatus) {
        [self cancelClear:panel_x-1 :panel_y];
    }
    //上判定
    if (currentStatus == upStatus) {
        [self cancelClear:panel_x :panel_y-1];
    }
    
}
-(int)searchPanel:(int)panel_x :(int)panel_y
{
    int currentStatus = self.panelStatus[panel_x][panel_y];
    //注目するセルがパネルでない場合、探索をしない。
    if (![self isPanel:currentStatus]) {
        return 10;
    }
    int upStatus = self.panelStatus[panel_x][panel_y-1];
    int downStatus = self.panelStatus[panel_x][panel_y+1];
    int rightStatus = self.panelStatus[panel_x+1][panel_y];
    int leftStatus = self.panelStatus[panel_x-1][panel_y];
    int count = 1;
   
    self.panelStatus[panel_x][panel_y]+=10;
    //色が同じかどうかを判定する
    //下判定
    if (currentStatus == downStatus) {
        if (self.panelStatus[panel_x][panel_y+2]==currentStatus
            || upStatus == currentStatus+10
            || upStatus == currentStatus) {
            count += [self searchPanel:panel_x :panel_y+1];
        }
    }
    //上判定
    if (currentStatus == upStatus) {
        if (self.panelStatus[panel_x][panel_y-2]==currentStatus
            || downStatus == currentStatus+10
            || downStatus == currentStatus) {
            count += [self searchPanel:panel_x :panel_y-1];
        }
    }
    //右判定
    if (currentStatus == rightStatus) {
        if (self.panelStatus[panel_x+2][panel_y]==currentStatus
            || leftStatus == currentStatus+10
            || leftStatus == currentStatus) {
            count += [self searchPanel:panel_x+1 :panel_y];
        }
    }
    //左判定
    if (currentStatus == leftStatus) {
        if (self.panelStatus[panel_x-2][panel_y]==currentStatus
            || rightStatus == currentStatus
            || rightStatus == currentStatus+10) {
            count += [self searchPanel:panel_x-1 :panel_y];
        }
    }
    return count;
}
-(BOOL)isPanel:(int)status
{
    if (status<10 && status >0) {
        return YES;
    }else{
        return NO;
    }
}
//同時消し判定
-(BOOL)isSimultaneousClear
{
    for (int i=1; i<panelHeight+1; i++) {
        for (int j=1; j<panelWidth+1; j++) {
            int connect = 0;
            connect = [self searchSimultaneous:j:i];
//            [self logPanel];
            if (connect >=3) {
                [self notifySimultaneous];
            }
//            [self logPanel];
        }
    }
    return YES;
}
-(BOOL)isClear:(int)status
{
    if (status>10 && status<20) {
        return YES;
    }
    return NO;
}
-(int)searchSimultaneous:(int)panel_x :(int)panel_y
{
    int currentStatus = self.panelStatus[panel_x][panel_y];
    //注目するセルが消去予定でない場合、探索をしない。
    if (![self isClear:currentStatus]) {
        return 0;
    }
    int rightStatus = self.panelStatus[panel_x+1][panel_y];
    int leftStatus = self.panelStatus[panel_x-1][panel_y];
    int upStatus = self.panelStatus[panel_x][panel_y-1];
    int downStatus = self.panelStatus[panel_x][panel_y+1];
    int count = 1;
    //チェック済みフラグをたてる▶︎消す
    self.panelStatus[panel_x][panel_y]=0;
    
    //消去予定のパネルかどうかを判定する。
    //右判定
    if (rightStatus>10 && rightStatus<20) {
        count += [self searchSimultaneous:panel_x+1 :panel_y];
    }
    //下判定
    if (downStatus>10 && downStatus<20) {
        count += [self searchSimultaneous:panel_x :panel_y+1];
    }
    //左判定
    if (leftStatus>10 && leftStatus<20) {
        count += [self searchSimultaneous:panel_x-1 :panel_y];
    }
    //上判定
    if (upStatus>10 && upStatus<20) {
        count += [self searchSimultaneous:panel_x :panel_y-1];
    }
    return count;
    
}
//--落下判定--
-(BOOL)fallCheck
{
    for (int i=1; i<panelWidth+1; i++) {
        for (int j=1; j<panelHeight+1; j++) {
            [self searchFall:i :j];
        }
    }
//    [self logPanel];
    return YES;
}
/**
 * パネルが落下中かどうかを調べる
 * ステータスが20台なら落下中
 */
-(BOOL)isFalling:(int)status
{
    if (status>20 && status<30) {
        return YES;
    }
    
    return NO;
}
/**
 * パネルが落ちるかどうかを調べる
 * 落ちる場合、ステータスを20台に上げる
 */
-(int)searchFall:(int)panel_x :(int)panel_y
{
    int currentStatus =self.panelStatus[panel_x][panel_y];
    //もし、自身が落下中なら何もしない
    if ([self isFalling:currentStatus]) {
        return 0;
    }
    //もし一つ上が落下中なら落下中にする
    if ([self isFalling:self.panelStatus[panel_x][panel_y-1]]) {
        self.panelStatus[panel_x][panel_y]+=20;
        return 0;
    }
    //もし自身が空白で、上が空白でないなら、何もしない
    if (currentStatus==0) {
        return 0;
    }
    //もし下に空白があるなら、落下中にする
    for (int i=panel_y; i<panelHeight+1; i++) {
        if (self.panelStatus[panel_x][i]==0) {
            self.panelStatus[panel_x][panel_y]+=20;
            break;
        }
    }
    
    return 0;
}
//--パネルの落下処理--
-(BOOL)fallPanel
{
    for (int i=1;i<panelWidth+1; i++) {
        for (int j=panelHeight; j>0; j--) {
            int currentStatus = self.panelStatus[i][j];
            if (self.panelStatus[i][j]>=20) {
                if (self.panelStatus[i][j-1]==0) {
                    self.panelStatus[i][j]=0;
                    continue;
                }
                if (self.panelStatus[i][j-1]==-1) {
                    self.panelStatus[i][j]=0;
                }else{
                self.panelStatus[i][j]=self.panelStatus[i][j-1]-20;
                }
            }
//            [self logPanel];
        }
    }
//    [self logPanel];
    return YES;
}
//--入れ替え--
-(void)panelSwap:(int)panel_x :(int)panel_y
{
    [self logPanel];
    if (panel_x>=panelWidth || panel_x<1) {
        NSLog(@"禁止されている入れ替え");
    }
    int tmp = self.panelStatus[panel_x][panel_y];
    self.panelStatus[panel_x][panel_y]=self.panelStatus[panel_x+1][panel_y];
    self.panelStatus[panel_x+1][panel_y] = tmp;
    [self logPanel];
    
}

//--ユーティリティ--
//GUIのコントローラーに変更を知らせる
-(void)notifySimultaneous
{
    
}
//
-(void)logPanel
{
    NSMutableString *output = [NSMutableString string];
    [output appendString:@"\n"];
    for (int i=0; i<panelHeight+2; i++) {
        for (int j=0; j<panelWidth+2; j++) {
            [output appendString:[NSString stringWithFormat:@"%2d,",self.panelStatus[j][i]]];
        }
        [output appendString:@"\n"];
    }
    
    NSLog(@"%@",output);
    
}

//デコンストラクタ
-(void)dealloc
{
    for (int i=0; i<panelWidth+2; i++) {
        free(self.panelStatus[i]);
    }
    free(self.panelStatus);
}
@end
